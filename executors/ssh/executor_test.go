package ssh

import (
	"context"
	"net"
	"testing"

	gliderSSH "github.com/gliderlabs/ssh"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	globalConfig "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/sshkey"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

func TestNew(t *testing.T) {
	provider, err := New(globalConfig.Global{}, logging.New())

	require.NoError(t, err)
	assert.IsType(t, &Executor{}, provider)
}

func TestExecute(t *testing.T) {
	ctx := context.Background()

	provider, err := New(globalConfig.Global{}, logging.New())
	require.NoError(t, err)

	t.Run("password auth", func(t *testing.T) {
		addr, cleanup := dummyListener(t, func(sess gliderSSH.Session) {
			assert.Equal(t, "gitlab", sess.User())
			assert.Equal(t, "test", sess.RawCommand())
			require.NoError(t, sess.Exit(0))
		}, gliderSSH.PasswordAuth(func(ctx gliderSSH.Context, pass string) bool {
			return assert.Equal(t, "secret", pass)
		}))
		defer cleanup()

		vmInst := vm.Instance{
			IPAddress: addr.String(),
			Username:  "gitlab",
			Password:  "secret",
		}

		err = provider.Execute(ctx, vmInst, []byte("test"))
		require.NoError(t, err)
	})

	t.Run("private key auth", func(t *testing.T) {
		priv, pub, err := sshkey.Generate()
		require.NoError(t, err)

		pubKey, _, _, _, err := gliderSSH.ParseAuthorizedKey(pub)
		require.NoError(t, err)

		addr, cleanup := dummyListener(t, func(sess gliderSSH.Session) {
			assert.Equal(t, "gitlab", sess.User())
			assert.Equal(t, "test", sess.RawCommand())
			require.NoError(t, sess.Exit(0))
		}, gliderSSH.PublicKeyAuth(func(ctx gliderSSH.Context, key gliderSSH.PublicKey) bool {
			return assert.True(t, gliderSSH.KeysEqual(key, pubKey))
		}))
		defer cleanup()

		vmInst := vm.Instance{
			IPAddress:     addr.String(),
			Username:      "gitlab",
			PrivateSSHKey: priv,
		}

		err = provider.Execute(ctx, vmInst, []byte("test"))
		require.NoError(t, err)
	})

	t.Run("invalid private key", func(t *testing.T) {
		vmInst := vm.Instance{
			IPAddress:     "127.0.0.1:22",
			Username:      "gitlab",
			PrivateSSHKey: []byte("not valid"),
		}

		err = provider.Execute(ctx, vmInst, []byte("test"))
		require.Error(t, err)
		assert.Contains(t, err.Error(), "parsing private key:")
	})

	t.Run("invalid vmInst", func(t *testing.T) {
		vmInst := vm.Instance{
			IPAddress: "127.0.0.1:22",
			Username:  "gitlab",
		}

		err = provider.Execute(ctx, vmInst, []byte("test"))
		require.Error(t, err)
		assert.Contains(t, err.Error(), "one of PrivateSSHKey or Password must be set")
	})

	t.Run("ssh connection issue", func(t *testing.T) {
		vmInst := vm.Instance{
			IPAddress: "127.0.0.1",
			Username:  "gitlab",
			Password:  "secret",
		}

		err = provider.Execute(ctx, vmInst, []byte("test"))
		require.Error(t, err)
		assert.Contains(t, err.Error(), "connect to ssh:")
	})

	t.Run("password auth and script failure", func(t *testing.T) {
		addr, cleanup := dummyListener(t, func(sess gliderSSH.Session) {
			require.NoError(t, sess.Exit(1))
		}, func(*gliderSSH.Server) error { return nil })
		defer cleanup()

		vmInst := vm.Instance{
			IPAddress: addr.String(),
			Username:  "gitlab",
			Password:  "secret",
		}

		err = provider.Execute(ctx, vmInst, []byte("test"))
		require.Error(t, err)
		assert.Contains(t, err.Error(), "run script:")
	})

	t.Run("context canceled during script run", func(t *testing.T) {
		ctx, cancel := context.WithCancel(ctx)
		defer cancel()

		addr, cleanup := dummyListener(t, func(sess gliderSSH.Session) {
			cancel()
		}, func(*gliderSSH.Server) error { return nil })
		defer cleanup()

		vmInst := vm.Instance{
			IPAddress: addr.String(),
			Username:  "gitlab",
			Password:  "secret",
		}

		err = provider.Execute(ctx, vmInst, []byte("test"))
		require.Error(t, err)
		assert.Contains(t, err.Error(), "run script: context canceled")
	})
}

func dummyListener(t *testing.T, handler func(gliderSSH.Session), option gliderSSH.Option) (net.Addr, func()) {
	l, err := net.Listen("tcp", "127.0.0.1:")
	require.NoError(t, err)

	srv := &gliderSSH.Server{}
	srv.Handle(handler)
	err = srv.SetOption(option)
	require.NoError(t, err)

	go func() {
		err = srv.Serve(l)
		assert.ErrorIs(t, err, gliderSSH.ErrServerClosed)
	}()

	cleanup := func() {
		err := srv.Close()
		require.NoError(t, err)
	}

	return l.Addr(), cleanup
}
