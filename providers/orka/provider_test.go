package orka

import (
	"context"
	"io/ioutil"
	"net"
	"os"
	"path/filepath"
	"testing"
	"time"

	gliderSSH "github.com/gliderlabs/ssh"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"golang.org/x/crypto/ssh"

	globalConfig "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/jobresponse/jobresponsetest"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	mockOrka "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/mocks/orka"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/orka"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/runner"
	mockExecutors "gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/mocks/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/providers/orka/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

func TestNew(t *testing.T) {
	type testCase struct {
		cfg         config.Provider
		expectedErr string
	}

	dir, err := ioutil.TempDir("", "")
	require.NoError(t, err)
	defer os.RemoveAll(dir)

	allowedProjectPath := filepath.Join(dir, "allowed_projects")
	require.NoError(t, ioutil.WriteFile(allowedProjectPath, []byte("/group/project"), 0777))

	testCases := map[string]testCase{
		"empty config": {
			cfg:         config.Provider{},
			expectedErr: "invalid Orka configuration: endpoint is required",
		},
		"invalid config": {
			cfg:         config.Provider{Endpoint: "http://1.2.3.4", Token: "foo", BaseImage: "foo", User: "foo", Password: "foo", Cores: 0},
			expectedErr: "invalid Orka configuration: cores must be 3, 4, 6, 8, 12, or 24",
		},
		"valid config": {
			cfg:         config.Provider{Endpoint: "http://1.2.3.4", Token: "foo", BaseImage: "foo", User: "foo", Password: "foo", Cores: 3},
			expectedErr: "",
		},
		"invalid allowed project list path": {
			cfg:         config.Provider{Endpoint: "http://1.2.3.4", Token: "foo", BaseImage: "foo", User: "foo", Password: "foo", Cores: 3, BetaAllowedProjectsPath: "/tmp/nope/a/b"},
			expectedErr: "invalid Orka configuration: allowed project path stat",
		},
		"valid allowed project list path": {
			cfg:         config.Provider{Endpoint: "http://1.2.3.4", Token: "foo", BaseImage: "foo", User: "foo", Password: "foo", Cores: 3, BetaAllowedProjectsPath: allowedProjectPath},
			expectedErr: "",
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			provider, err := New(globalConfig.Global{Orka: tc.cfg}, logging.New())

			if tc.expectedErr == "" {
				require.NoError(t, err)
				assert.IsType(t, &Provider{}, provider)
			} else {
				assert.Contains(t, err.Error(), tc.expectedErr)
			}
		})
	}
}

func TestAllowedImages(t *testing.T) {
	cfg := config.Provider{Endpoint: "http://1.2.3.4", Token: "foo", BaseImage: "foo", User: "foo", Password: "foo", Cores: 3}
	cfg.AllowedImages = []string{"a", "b"}
	cfg.ImageAliases = map[string]string{"c": "d", "e": "f"}

	provider, err := New(globalConfig.Global{Orka: cfg}, logging.New())
	require.NoError(t, err)

	assert.ElementsMatch(t, provider.(*Provider).cfg.AllowedImages, []string{"a", "b", "c", "e"})
}

func prepareTestProvider(t *testing.T) (*mockOrka.Client, providers.Provider) {
	mockClient := &mockOrka.Client{}
	mockClient.Test(t)

	provider := &Provider{
		cfg: config.Provider{
			BaseImage: "foo",
			Cores:     3,
			User:      "gitlab",
			Password:  "gitlab",
		},
		bootTimeout: 5 * time.Second,
		logger:      logging.New(),
		client:      mockClient,
	}

	return mockClient, provider
}

func TestVMName(t *testing.T) {
	provider := &Provider{
		vmTag: "test-mAchInE-tag",
	}

	runnerData := vm.NameRunnerData{
		RunnerShortToken: "a1b2C3D4",
		ProjectURL:       "https://gitlab.example.com/namespace/project",
		PipelineID:       1234,
		JobID:            4321,
	}

	expectedName := "runner-test-machine-tag-4321"

	assert.Equal(t, expectedName, provider.VMName(runnerData))
}

func TestVMNameForRunner(t *testing.T) {
	provider := &Provider{
		vmTag: "test-mAchInE-tag",
		cfg: config.Provider{
			DeployFromConfig: false,
		},
	}

	runnerData := vm.NameRunnerData{
		RunnerShortToken: "a1b2C3D4",
		ProjectURL:       "https://gitlab.example.com/namespace/project",
		PipelineID:       1234,
		JobID:            4321,
	}

	expectedName := "runner-test-machine-tag-4321"
	assert.Equal(t, expectedName, provider.VMNameForRunner(runnerData))

	provider.cfg.DeployFromConfig = true
	assert.Equal(t, "", provider.VMNameForRunner(runnerData))
}

func TestCreate(t *testing.T) {
	ctx := context.Background()

	t.Run("normal case", func(t *testing.T) {
		mockClient, provider := prepareTestProvider(t)
		mockExecutor := &mockExecutors.Executor{}

		addr, cleanup := dummyListener(t)
		defer cleanup()

		orkaInstance := &orka.Instance{
			ID:   "abcde",
			Addr: addr.(*net.TCPAddr),
		}

		vmInst := &vm.Instance{Name: "vm"}

		mockClient.On("CreateVM", ctx, "vm", "foo.img", 3).Return(nil)
		mockClient.On("DeployVM", ctx, "vm").Return(orkaInstance, nil)
		mockExecutor.On("Execute", ctx, mock.MatchedBy(func(v vm.Instance) bool {
			return assert.Equal(t, vmInst.Name, v.Name) &&
				assert.Equal(t, orkaInstance.Addr.String(), v.IPAddress) &&
				assert.Equal(t, "gitlab", v.Username) &&
				assert.Equal(t, "gitlab", v.Password)
		}), mock.IsType([]byte{})).Return(nil)

		err := provider.Create(ctx, mockExecutor, vmInst)
		require.NoError(t, err)

		assert.Empty(t, vmInst.Password)
		_, err = ssh.ParsePrivateKey(vmInst.PrivateSSHKey)
		require.NoError(t, err)
	})

	t.Run("create fails", func(t *testing.T) {
		mockClient, provider := prepareTestProvider(t)

		mockClient.On("CreateVM", ctx, "vm", "foo.img", 3).Return(assert.AnError)

		err := provider.Create(ctx, nil, &vm.Instance{Name: "vm"})
		assert.ErrorIs(t, err, assert.AnError)
		assert.Contains(t, err.Error(), "unable to create VM: ")
	})

	t.Run("deploy fails", func(t *testing.T) {
		mockClient, provider := prepareTestProvider(t)

		mockClient.On("CreateVM", ctx, "vm", "foo.img", 3).Return(nil)
		mockClient.On("DeployVM", ctx, "vm").Return(nil, assert.AnError)

		err := provider.Create(ctx, nil, &vm.Instance{Name: "vm"})
		assert.ErrorIs(t, err, assert.AnError)
		assert.Contains(t, err.Error(), "unable to deploy VM: ")
	})

	t.Run("top level context is cancelled", func(t *testing.T) {
		mockClient, provider := prepareTestProvider(t)

		// not setting up the dummy listener so the dial loop times out
		orkaInstance := &orka.Instance{
			ID: "abcde",
			Addr: &net.TCPAddr{
				IP:   nil,
				Port: 0,
			},
		}

		newCtx, cancel := context.WithCancel(ctx)
		cancel() // cancel immediately

		mockClient.On("CreateVM", newCtx, "vm", "foo.img", 3).Return(nil)
		mockClient.On("DeployVM", newCtx, "vm").Return(orkaInstance, nil)

		err := provider.Create(newCtx, nil, &vm.Instance{Name: "vm"})
		assert.ErrorIs(t, err, context.Canceled)
		assert.Contains(t, err.Error(), "VM failed to boot in time: ")
	})

	t.Run("boot timeout is reached", func(t *testing.T) {
		mockClient, provider := prepareTestProvider(t)

		// not setting up the dummy listener so the dial loop times out
		orkaInstance := &orka.Instance{
			ID: "abcde",
			Addr: &net.TCPAddr{
				IP:   nil,
				Port: 0,
			},
		}

		mockClient.On("CreateVM", ctx, "vm", "foo.img", 3).Return(nil)
		mockClient.On("DeployVM", ctx, "vm").Return(orkaInstance, nil)

		err := provider.Create(ctx, nil, &vm.Instance{Name: "vm"})
		assert.ErrorIs(t, err, context.DeadlineExceeded)
		assert.Contains(t, err.Error(), "VM failed to boot in time: ")
	})

	t.Run("executor fails", func(t *testing.T) {
		mockClient, provider := prepareTestProvider(t)
		mockExecutor := &mockExecutors.Executor{}

		addr, cleanup := dummyListener(t)
		defer cleanup()

		orkaInstance := &orka.Instance{
			ID:   "abcde",
			Addr: addr.(*net.TCPAddr),
		}

		mockClient.On("CreateVM", ctx, "vm", "foo.img", 3).Return(nil)
		mockClient.On("DeployVM", ctx, "vm").Return(orkaInstance, nil)
		mockExecutor.On("Execute", ctx, mock.Anything, mock.Anything).Return(assert.AnError)

		err := provider.Create(ctx, mockExecutor, &vm.Instance{Name: "vm"})
		assert.ErrorIs(t, err, assert.AnError)
		assert.Contains(t, err.Error(), "executing first run script: ")
	})

	t.Run("create allowed project rejected", func(t *testing.T) {
		_, provider := prepareTestProvider(t)
		provider.(*Provider).allowedProjects = []string{"/group/subgroup/nope"}

		os.Setenv("BUILD_FAILURE_EXIT_CODE", "1")
		os.Setenv("SYSTEM_FAILURE_EXIT_CODE", "2")
		os.Setenv("CUSTOM_ENV_CI_PROJECT_URL", "https://gitlab.com/group/subgroup/project")

		jobresponsetest.CreateJobResponseFile(t, 123)
		defer jobresponsetest.RemoveJobResponseFile(t)

		require.NoError(t, runner.InitAdapter())

		err := provider.Create(ctx, nil, &vm.Instance{Name: "vm"})
		assert.ErrorIs(t, err, &runner.BuildFailureError{})
	})

	t.Run("create allowed project accepted", func(t *testing.T) {
		mockClient, provider := prepareTestProvider(t)
		mockExecutor := &mockExecutors.Executor{}

		provider.(*Provider).allowedProjects = []string{"/group/subgroup/project"}

		os.Setenv("BUILD_FAILURE_EXIT_CODE", "1")
		os.Setenv("SYSTEM_FAILURE_EXIT_CODE", "2")
		os.Setenv("CUSTOM_ENV_CI_PROJECT_URL", "https://gitlab.com/group/subgroup/project")

		jobresponsetest.CreateJobResponseFile(t, 123)
		defer jobresponsetest.RemoveJobResponseFile(t)

		require.NoError(t, runner.InitAdapter())

		addr, cleanup := dummyListener(t)
		defer cleanup()

		orkaInstance := &orka.Instance{
			ID:   "abcde",
			Addr: addr.(*net.TCPAddr),
		}

		mockClient.On("CreateVM", ctx, "vm", "foo.img", 3).Return(nil)
		mockClient.On("DeployVM", ctx, "vm").Return(orkaInstance, nil)
		mockExecutor.On("Execute", ctx, mock.Anything, mock.Anything).Return(nil)

		err := provider.Create(ctx, mockExecutor, &vm.Instance{Name: "vm"})
		assert.NoError(t, err)
	})
}

func TestGet(t *testing.T) {
	ctx := context.Background()

	t.Run("get is disabled", func(t *testing.T) {
		_, provider := prepareTestProvider(t)

		_, err := provider.Get(ctx, "vm")
		require.Error(t, err)
		assert.Contains(t, err.Error(), "Ensure the ProviderCache is configured")
	})
}

func TestDelete(t *testing.T) {
	ctx := context.Background()

	t.Run("normal case", func(t *testing.T) {
		mockClient, provider := prepareTestProvider(t)

		mockClient.On("PurgeVM", ctx, "vm").Return(nil)

		err := provider.Delete(ctx, vm.Instance{Name: "vm", Orka: config.Instance{ID: "123"}})
		require.NoError(t, err)
	})

	t.Run("purge fails", func(t *testing.T) {
		mockClient, provider := prepareTestProvider(t)

		mockClient.On("PurgeVM", ctx, "vm").Return(assert.AnError)

		err := provider.Delete(ctx, vm.Instance{Name: "vm", Orka: config.Instance{ID: "123"}})
		assert.ErrorIs(t, err, assert.AnError)
		assert.Contains(t, err.Error(), "unable to delete VM: ")
	})

	t.Run("deleting VM that wasn't created quietly returns", func(t *testing.T) {
		_, provider := prepareTestProvider(t)

		err := provider.Delete(ctx, vm.Instance{Name: "vm", Orka: config.Instance{}})
		assert.NoError(t, err)
	})
}

func dummyListener(t *testing.T) (net.Addr, func()) {
	l, err := net.Listen("tcp", "127.0.0.1:")
	require.NoError(t, err)

	srv := &gliderSSH.Server{}
	go func() {
		err = srv.Serve(l)
		assert.ErrorIs(t, err, gliderSSH.ErrServerClosed)
	}()

	cleanup := func() {
		err := srv.Close()
		require.NoError(t, err)
	}

	return l.Addr(), cleanup
}
