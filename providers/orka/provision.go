package orka

import (
	"crypto/rand"
	"encoding/hex"
	"fmt"
	"strings"
	"text/template"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/sshkey"
)

// firstRunScript will:
//   - generate a single-use random SSH keypair for executing the build
//   - generate a single-use random password for the user
//   - set up the VM to accept the new public key for authentication
//   - replace the built-in user password by the new random password
//   - ... at this point only this autoscaler process knows how to authenticate to the VM ...
//   - return the new private key
func firstRunScript(password string) (string, []byte, error) {
	script := `
	# http://redsymbol.net/articles/unofficial-bash-strict-mode/
	set -euo pipefail

	echo "Authorizing session SSH keypair..."
	mkdir -p ~/.ssh/
	chmod 0700 ~/.ssh
	echo "{{.PublicKey}}" > ~/.ssh/authorized_keys
	chmod 0600 ~/.ssh/authorized_keys

	# disabling history by prepending commands with a space
	echo "Randomizing user password and resetting Keychain..."
	setopt histignorespace
	 echo "{{.OldPassword}}" | sudo -S /usr/bin/dscl . -passwd "$HOME" "{{.OldPassword}}" "{{.NewPassword}}"
	 echo "{{.NewPassword}}" | sudo -S rm -rf "$HOME/Library/Keychains/*"

	# Add line break after "Password:" prompt
	echo
	`

	tmpl, err := template.New("provision").Parse(script)
	if err != nil {
		return "", nil, fmt.Errorf("parse script template: %w", err)
	}

	newPassword, err := randPassword()
	if err != nil {
		return "", nil, fmt.Errorf("generate random password: %w", err)
	}

	priv, pub, err := sshkey.Generate()
	if err != nil {
		return "", nil, fmt.Errorf("generate session key: %w", err)
	}

	var b strings.Builder
	err = tmpl.Execute(&b, struct {
		PublicKey   string
		OldPassword string
		NewPassword string
	}{
		PublicKey:   strings.TrimSpace(string(pub)),
		OldPassword: password,
		NewPassword: newPassword,
	})
	if err != nil {
		return "", nil, fmt.Errorf("generate script from template: %w", err)
	}

	return b.String(), priv, nil
}

// randPassword returns a random 128-bit hexadecimal string.
func randPassword() (string, error) {
	buf := make([]byte, 16)
	_, err := rand.Read(buf)

	return hex.EncodeToString(buf), err
}
