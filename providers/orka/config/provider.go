package config

import (
	"errors"
	"fmt"
	"net/url"
	"os"
)

type Provider struct {
	Endpoint      string            `toml:"Endpoint"`
	Token         string            `toml:"Token"`
	BaseImage     string            `toml:"BaseImage"`
	User          string            `toml:"User"`
	Password      string            `toml:"Password"`
	AllowedImages []string          `toml:"AllowedImages"`
	Cores         int               `toml:"Cores"`
	HTTPTimeout   int               `toml:"HTTPTimeout"`
	BootTimeout   int               `toml:"BootTimeout"`
	ImageAliases  map[string]string `toml:"ImageAliases"`

	BetaAllowedProjectsPath string `toml:"BetaAllowedProjectsPath"`

	DeployFromConfig bool `toml:"DeployFromConfig"`
}

func (cfg Provider) Validate() error {
	if cfg.Endpoint == "" {
		return errors.New("endpoint is required")
	}

	if cfg.Token == "" {
		return errors.New("token is required")
	}

	if cfg.User == "" {
		return errors.New("user is required")
	}

	if cfg.Password == "" {
		return errors.New("password is required")
	}

	if !cfg.DeployFromConfig {
		switch cfg.Cores {
		case 3, 4, 6, 8, 12, 24:
			break
		default:
			return errors.New("cores must be 3, 4, 6, 8, 12, or 24")
		}
	}

	endpoint, err := url.Parse(cfg.Endpoint)
	if err != nil {
		return fmt.Errorf("endpoint URL is not valid: %w", err)
	}

	if endpoint.Scheme != "http" {
		return errors.New("endpoint should start with http://")
	}

	if cfg.BetaAllowedProjectsPath != "" {
		_, err := os.Stat(cfg.BetaAllowedProjectsPath)
		if err != nil {
			return fmt.Errorf("allowed project path stat: %w", err)
		}
	}

	return nil
}
