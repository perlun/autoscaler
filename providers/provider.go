package providers

import (
	"context"
	"fmt"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/config"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/executors"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/factories"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

const factoryType = "provider"

type Provider interface {
	Get(ctx context.Context, name string) (vm.Instance, error)
	Create(ctx context.Context, e executors.Executor, vmInst *vm.Instance) error
	Delete(ctx context.Context, vmInst vm.Instance) error
	VMName(runnerData vm.NameRunnerData) string
	VMNameForRunner(runnerData vm.NameRunnerData) string
}

type Factory func(cfg config.Global, logger logging.Logger) (Provider, error)

var factoriesRegistry = factories.NewRegistry(factoryType)

func MustRegister(name string, f Factory) {
	factoriesRegistry.MustRegister(name, f)
}

func Factorize(cfg config.Global, logger logging.Logger) (Provider, error) {
	factory, err := factoriesRegistry.Get(cfg.Provider)
	if err != nil {
		return nil, err
	}

	p, err := factory.(Factory)(cfg, logger)
	if err != nil {
		return nil, fmt.Errorf("couldn't factorize provider: %w", err)
	}

	return newCacheProviderDecorator(cfg, logger, p), nil
}
