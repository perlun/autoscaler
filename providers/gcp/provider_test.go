package gcp

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/vm"
)

func TestVMName(t *testing.T) {
	provider := &Provider{
		vmTag: "test-mAchInE-tag",
	}
	runnerData := vm.NameRunnerData{
		RunnerShortToken: "a1b2C3D4",
		ProjectURL:       "https://gitlab.example.com/namespace/project",
		PipelineID:       1234,
		JobID:            4321,
	}

	expectedName := "runner-a1b2c3d4-test-machine-tag-8838f262c67ed8272ad9"

	assert.Equal(t, expectedName, provider.VMName(runnerData))
}

func TestVMNameForRunner(t *testing.T) {
	provider := &Provider{
		vmTag: "test-mAchInE-tag",
	}
	runnerData := vm.NameRunnerData{
		RunnerShortToken: "a1b2C3D4",
		ProjectURL:       "https://gitlab.example.com/namespace/project",
		PipelineID:       1234,
		JobID:            4321,
	}

	expectedName := "runner-a1b2c3d4-test-machine-tag-8838f262c67ed8272ad9"

	assert.Equal(t, expectedName, provider.VMNameForRunner(runnerData))
}
