package windows

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"time"
)

const (
	credentialsKeyLength = 2048
)

type credentialsResponse struct {
	ErrorMessage      string `json:"errorMessage,omitempty"`
	EncryptedPassword string `json:"encryptedPassword,omitempty"`
	Modulus           string `json:"modulus,omitempty"`
}

type windowsKey struct {
	ExpireOn string `json:"ExpireOn,omitempty"`
	Exponent string `json:"Exponent,omitempty"`
	Modulus  string `json:"Modulus,omitempty"`
	UserName string `json:"UserName,omitempty"`
}

type Credentials struct {
	key  *rsa.PrivateKey
	data *windowsKey

	encodedPassword string
}

func NewCredentials(username string) (*Credentials, error) {
	key, err := rsa.GenerateKey(rand.Reader, credentialsKeyLength)
	if err != nil {
		return nil, fmt.Errorf("couldn't create RSA key pair: %w", err)
	}

	bs := make([]byte, 4)
	binary.BigEndian.PutUint32(bs, uint32(key.E))

	ki := &Credentials{
		key: key,
		data: &windowsKey{
			ExpireOn: time.Now().Add(5 * time.Minute).Format(time.RFC3339),
			Exponent: base64.StdEncoding.EncodeToString(bs),
			Modulus:  base64.StdEncoding.EncodeToString(key.N.Bytes()),
			UserName: username,
		},
	}

	return ki, nil
}

func (c *Credentials) WindowsKey() (string, error) {
	windowsKeyRaw, err := json.Marshal(c.data)
	if err != nil {
		return "", fmt.Errorf("couldn't JSON-encode Windows key: %w", err)
	}

	return string(windowsKeyRaw), nil
}

func (c *Credentials) ScanEncryptedPassword(input []byte) (bool, error) {
	var response credentialsResponse

	err := json.Unmarshal(input, &response)
	if err != nil {
		return false, nil
	}

	if response.Modulus != c.data.Modulus {
		return false, errors.New("invalid key Modulus")
	}

	if response.ErrorMessage != "" {
		return false, fmt.Errorf("error from Windows agent: %s", response.ErrorMessage)
	}

	c.encodedPassword = response.EncryptedPassword

	return true, nil
}

func (c *Credentials) DecodeAndDecryptPassword() (string, error) {
	encryptedPassword, err := base64.StdEncoding.DecodeString(c.encodedPassword)
	if err != nil {
		return "", fmt.Errorf("couldn't decode password: %w", err)
	}

	password, err := rsa.DecryptOAEP(sha1.New(), rand.Reader, c.key, encryptedPassword, nil)
	if err != nil {
		return "", fmt.Errorf("couldn't decrypt password: %w", err)
	}

	return string(password), nil
}
