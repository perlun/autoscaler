$ErrorActionPreference = "Stop"; # Make all errors terminating
Set-Item -Path Env:GOPATH -Value $env:USERPROFILE\go
$TestDir = ".tests"

New-Item -Type Directory -Path $TestDir -ErrorAction SilentlyContinue

go install github.com/jstemmer/go-junit-report

if (Test-Path env:GO_TEST_RACE)
{
    go test -v -race ./... | Tee-Object -File $TestDir/output-windows-race.txt
    $GoTestExitCode = $LASTEXITCODE
    Get-Content $TestDir/output-windows-race.txt | ~\go\bin\go-junit-report.exe | Out-File $TestDir/junit-windows-race.xml

    # Make sure that we fail the script if tests fail.
    exit $GoTestExitCode
}

go test -v ./... | Tee-Object -File $TestDir/output-windows.txt
$GoTestExitCode = $LASTEXITCODE
Get-Content $TestDir/output-windows.txt | ~\go\bin\go-junit-report.exe | Out-File $TestDir/junit-windows.xml

# Make sure that we fail the script if tests fail.
exit $GoTestExitCode
