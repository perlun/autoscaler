#!/usr/bin/env bash
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
IFS=$'\n\t'
set -euo pipefail

# To run this manually, you need:
#
# eval "$(op signin gitlab.1password.com)"
# export ORKA_VPN_USER="$(op get item --vault Verify --account gitlab --fields username "Orka VPN")"
# export ORKA_VPN_PASS="$(op get item --vault Verify --account gitlab --fields password "Orka VPN")"
# export ORKA_VPN_CERT="$(op get item --vault Verify --account gitlab --fields "Cert" "Orka VPN")"
# export ORKA_VPN_IP="$(op get item --vault Verify --account gitlab --fields url "Orka VPN")"
# export ORKA_API_ENDPOINT="$(op get item --vault Verify --account gitlab --fields "API URL" "Orka VPN")"
# export ORKA_VM_PASSWORD="$(op get item --vault Verify --account gitlab --fields password "orka base VM")"
# export ORKA_API_TOKEN=$(jq -r ".token" ~/.config/configstore/orka-cli.json)

killvpn() {
  killall -SIGINT openconnect
}

echo "Connecting to MacStadium VPN..."
echo "$ORKA_VPN_PASS" | openconnect "$ORKA_VPN_IP" --background --protocol=anyconnect --user="$ORKA_VPN_USER" --servercert "$ORKA_VPN_CERT" --non-inter --passwd-on-stdin --timestamp
trap killvpn EXIT

# Wait for VPN to finish establishing connection
sleep 5

(

  cd scripts/orka_integration || exit 1

  sed -i "s|ORKA_API_ENDPOINT|$ORKA_API_ENDPOINT|" config.toml
  sed -i "s|ORKA_API_TOKEN|$ORKA_API_TOKEN|"       config.toml

  echo "Run autoscaler ..."

  export JOB_RESPONSE_FILE="$(pwd)/job_response.json"
  echo "{\"jobID\":\"${CI_JOB_ID}\"}" > "${JOB_RESPONSE_FILE}"

  export CUSTOM_ENV_CI_JOB_ID="$CI_JOB_ID"
  export BUILD_FAILURE_EXIT_CODE=1
  export SYSTEM_FAILURE_EXIT_CODE=2
  export CUSTOM_ENV_CI_JOB_IMAGE=macos-11-xcode-12

  cleanup() {
    VM_NAME="runner-autoscaler-$CUSTOM_ENV_CI_JOB_ID"
    curl --location --request DELETE "$ORKA_API_ENDPOINT/resources/vm/purge" --header 'Content-Type: application/json' --header "Authorization: Bearer $ORKA_API_TOKEN" --data-raw "{\"orka_vm_name\": \"$VM_NAME\"  }"
  }
  trap 'cleanup' ERR

  ../../build/autoscaler-linux-amd64 custom prepare

  # Wait for SSH to finish booting
  sleep 5

  ../../build/autoscaler-linux-amd64 custom run testscript.sh test | tee output.log
  ../../build/autoscaler-linux-amd64 custom run testscript.sh second_test # run is executed multiple times
  ../../build/autoscaler-linux-amd64 custom cleanup

  grep "All your base are belong to us" output.log

)
