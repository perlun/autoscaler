package backoff

import (
	"errors"
	"time"

	"github.com/cenkalti/backoff"

	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/logging"
)

type Settings struct {
	InitialInterval     int
	RandomizationFactor float64
	Multiplier          float64
	MaxInterval         int
	MaxElapsedTime      int
}

var ErrMaximumBackOffTimeElapsedExceeded = errors.New("maximum backOff elapsed time exceeded")

func Loop(settings Settings, logger logging.Logger, handler func(logger logging.Logger) (bool, error)) error {
	b := newBackOff(settings)
	tries := 0

	for {
		log := logger.WithField("retry-count", tries)

		shouldBreak, err := handler(log)
		if err != nil {
			log.
				WithError(err).
				Error("Error while handling backOff loop")
		}

		if shouldBreak {
			break
		}

		sleep := b.NextBackOff()
		if sleep == backoff.Stop {
			return ErrMaximumBackOffTimeElapsedExceeded
		}

		log.
			WithField("retry-interval", sleep).
			Debug("Waiting before next retry")

		time.Sleep(sleep)
		tries++
	}

	return nil
}

func newBackOff(settings Settings) backoff.BackOff {
	b := &backoff.ExponentialBackOff{
		InitialInterval:     time.Duration(settings.InitialInterval) * time.Second,
		RandomizationFactor: settings.RandomizationFactor,
		Multiplier:          settings.Multiplier,
		MaxInterval:         time.Duration(settings.MaxInterval) * time.Second,
		MaxElapsedTime:      time.Duration(settings.MaxElapsedTime) * time.Second,
		Clock:               backoff.SystemClock,
	}
	b.Reset()

	return b
}
