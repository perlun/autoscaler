package jobresponse

import (
	"encoding/json"
	"fmt"
	"os"

	"gitlab.com/gitlab-org/gitlab-runner/executors/custom/api"
)

// JobResponse is targeted subset of JobResponse from
// gitlab-runner/common/network.go, containing only ID.
// https://gitlab.com/gitlab-org/gitlab-runner/-/blob/489b248d2c68caaf432359fcb9d2415cb6f73f0b/common/network.go#L402
// Importing the whole gitlab-runner/config brings a large dependency
// graph and associated risk. However if we start depending on more of
// the JobResponse here we should just import it to avoid duplication.
type JobResponse struct {
	ID int64
}

func GetJobID() (int64, error) {
	jobResponseFile := os.Getenv(api.JobResponseFileVariable)
	if jobResponseFile == "" {
		return -1, fmt.Errorf("%v is not provided", api.JobResponseFileVariable)
	}

	file, err := os.Open(jobResponseFile)
	if err != nil {
		return -1, fmt.Errorf("opening %s: %w", jobResponseFile, err)
	}
	defer file.Close()

	var jobResponse JobResponse

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&jobResponse)
	if err != nil {
		return -1, fmt.Errorf("decoding job response: %w", err)
	}

	return jobResponse.ID, nil
}
