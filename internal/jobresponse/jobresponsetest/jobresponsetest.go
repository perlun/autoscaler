package jobresponsetest

import (
	"encoding/json"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler/internal/jobresponse"
	"gitlab.com/gitlab-org/gitlab-runner/executors/custom/api"
)

func CreateJobResponseFile(t *testing.T, jobID int64) string {
	f, err := os.CreateTemp("", "job_response_file.json")
	require.NoError(t, err, "error creating job response file")

	jobResponseFile := jobresponse.JobResponse{
		ID: jobID,
	}

	encoder := json.NewEncoder(f)
	err = encoder.Encode(&jobResponseFile)
	require.NoError(t, err, "error serializing job response")

	require.NoError(t, f.Close(), "error closing job response file")

	os.Setenv(api.JobResponseFileVariable, f.Name())

	return f.Name()
}

func RemoveJobResponseFile(t *testing.T) {
	jobResponseFile := os.Getenv(api.JobResponseFileVariable)
	assert.NotEmpty(t, jobResponseFile, "job response file not set")
	assert.NoError(t, os.Remove(jobResponseFile), "error removing job response file")
	os.Setenv(api.JobResponseFileVariable, "")
}
