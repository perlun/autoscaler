package orka

import (
	"context"
	"errors"
	"fmt"
	"net"
	"net/http"
	"strconv"
)

type vmStatusRespBody struct {
	respBaseBody

	VirtualMachineResources []vmResource `json:"virtual_machine_resources"`
}

type vmResource struct {
	VirtualMachineName string             `json:"virtual_machine_name"`
	VMDeploymentStatus string             `json:"vm_deployment_status"`
	Status             []vmResourceStatus `json:"status"`
}

type vmResourceStatus struct {
	Owner                 string `json:"owner"`
	VirtualMachineName    string `json:"virtual_machine_name"`
	VirtualMachineID      string `json:"virtual_machine_id"`
	NodeLocation          string `json:"node_location"`
	NodeStatus            string `json:"node_status"`
	VirtualMachineIP      string `json:"virtual_machine_ip"`
	VncPort               string `json:"vnc_port"`
	ScreenSharingPort     string `json:"screen_sharing_port"`
	SSHPort               string `json:"ssh_port"`
	CPU                   int    `json:"cpu"`
	Vcpu                  int    `json:"vcpu"`
	RAM                   string `json:"RAM"`
	BaseImage             string `json:"base_image"`
	Image                 string `json:"image"`
	ConfigurationTemplate string `json:"configuration_template"`
	VMStatus              string `json:"vm_status"`
}

var ErrVMNotFound = errors.New("VM not found")

func (c *client) GetSSHEndpoint(ctx context.Context, name string) (net.Addr, error) {
	var vmStatus vmStatusRespBody
	endpoint := getVMEndpointRelURL(endpointStatus, name)
	err := c.sendHTTPRequestWithRetries(ctx, http.MethodGet, endpoint, nil, &vmStatus)
	if err != nil {
		return nil, err
	}

	// Search for first VM with matching name in VirtualMachineResources
	for _, vmRes := range vmStatus.VirtualMachineResources {
		if vmRes.VirtualMachineName != name {
			continue
		}

		if len(vmRes.Status) == 0 {
			// If the array is empty, that means that a VM configuration exists
			// but no VM is deployed under this name
			continue
		}

		// If more than one VM with the same name is deployed, return the first one
		vmPort := vmRes.Status[0].SSHPort
		vmIP := vmRes.Status[0].VirtualMachineIP

		port, err := strconv.Atoi(vmPort)
		if err != nil {
			return nil, NewInvalidResponseError(fmt.Sprintf("parse SSH port %q", vmPort), err)
		}

		ip := net.ParseIP(vmIP)
		if ip == nil {
			return nil, NewInvalidResponseError(fmt.Sprintf("invalid IP format in %q", vmIP), nil)
		}

		return &net.TCPAddr{IP: ip, Port: port}, nil
	}

	return nil, ErrVMNotFound
}
