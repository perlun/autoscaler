package orka

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"strconv"
)

type deployVMReqBody struct {
	VMName string `json:"orka_vm_name"`
}

type deployVMResBody struct {
	ID      string `json:"vm_id"`
	IP      string `json:"ip"`
	SSHPort string `json:"ssh_port"`
}

type Instance struct {
	ID   string
	Addr *net.TCPAddr
}

func (c *client) DeployVM(ctx context.Context, name string) (*Instance, error) {
	endpoint := getVMEndpointRelURL(endpointDeploy)
	reqBody := deployVMReqBody{
		VMName: name,
	}
	resBody := &deployVMResBody{}
	err := c.sendHTTPRequestWithRetries(ctx, http.MethodPost, endpoint, reqBody, resBody)
	if err != nil {
		return nil, err
	}

	port, err := strconv.Atoi(resBody.SSHPort)
	if err != nil {
		return nil, NewInvalidResponseError(fmt.Sprintf("parse SSH port %q", resBody.SSHPort), err)
	}

	ip := net.ParseIP(resBody.IP)
	if ip == nil {
		return nil, NewInvalidResponseError(fmt.Sprintf("invalid IP format in %q", resBody.IP), nil)
	}

	return &Instance{
		ID:   resBody.ID,
		Addr: &net.TCPAddr{IP: ip, Port: port},
	}, nil
}
