package orka

import (
	"errors"
	"fmt"
	"net/http"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHTTPError(t *testing.T) {
	const endpoint1 = "some endpoint 1"
	const errorMessage = "an error occurred"
	errs := []string{"1", "2"}
	httpErr := HTTPError{Endpoint: endpoint1, StatusCode: http.StatusOK, Message: errorMessage, Errors: errs}

	testCases := map[string]struct {
		err             *HTTPError
		expectedMessage string
		expectedMatch   bool
	}{
		"matching_error": {
			err: &HTTPError{
				Endpoint:   endpoint1,
				StatusCode: http.StatusOK,
				Message:    errorMessage,
				Errors:     errs,
			},
			expectedMessage: fmt.Sprintf(
				"response from %q, status: %d, message: %s, errors: %s",
				endpoint1,
				http.StatusOK,
				errorMessage,
				strings.Join(errs, "; ")),
			expectedMatch: true,
		},
		"mismatched endpoint": {
			err: &HTTPError{
				Endpoint:   "some endpoint",
				StatusCode: http.StatusOK,
				Message:    errorMessage,
				Errors:     errs,
			},
			expectedMessage: fmt.Sprintf(
				"response from %q, status: %d, message: %s, errors: %s",
				"some endpoint",
				http.StatusOK,
				errorMessage,
				strings.Join(errs, "; ")),
			expectedMatch: false,
		},
		"mismatched error values": {
			err: &HTTPError{
				Endpoint:   endpoint1,
				StatusCode: http.StatusOK,
				Message:    errorMessage,
				Errors:     []string{"1", "3"},
			},
			expectedMessage: fmt.Sprintf(
				"response from %q, status: %d, message: %s, errors: 1; 3",
				endpoint1,
				http.StatusOK,
				errorMessage),
			expectedMatch: false,
		},
		"mismatched error count": {
			err: &HTTPError{
				Endpoint:   endpoint1,
				StatusCode: http.StatusOK,
				Message:    errorMessage,
				Errors:     []string{"1"},
			},
			expectedMessage: fmt.Sprintf(
				"response from %q, status: %d, message: %s, errors: 1",
				endpoint1,
				http.StatusOK,
				errorMessage),
			expectedMatch: false,
		},
		"mismatched empty errors": {
			err: &HTTPError{
				Endpoint:   endpoint1,
				StatusCode: http.StatusOK,
				Message:    errorMessage,
				Errors:     []string{},
			},
			expectedMessage: fmt.Sprintf(
				"response from %q, status: %d, message: %s",
				endpoint1,
				http.StatusOK,
				errorMessage),
			expectedMatch: false,
		},
		"mismatched nil errors": {
			err: &HTTPError{
				Endpoint:   endpoint1,
				StatusCode: http.StatusOK,
				Message:    errorMessage,
				Errors:     nil,
			},
			expectedMessage: fmt.Sprintf(
				"response from %q, status: %d, message: %s",
				endpoint1,
				http.StatusOK,
				errorMessage),
			expectedMatch: false,
		},
		"mismatched message": {
			err: &HTTPError{
				Endpoint:   endpoint1,
				StatusCode: http.StatusOK,
				Message:    "some other error",
				Errors:     errs,
			},
			expectedMessage: fmt.Sprintf(
				"response from %q, status: %d, message: some other error, errors: %s",
				endpoint1,
				http.StatusOK,
				strings.Join(errs, "; ")),
			expectedMatch: false,
		},
		"mismatched status code": {
			err: &HTTPError{
				Endpoint:   endpoint1,
				StatusCode: http.StatusForbidden,
				Message:    errorMessage,
				Errors:     errs,
			},
			expectedMessage: fmt.Sprintf(
				"response from %q, status: %d, message: %s, errors: %s",
				endpoint1,
				http.StatusForbidden,
				errorMessage,
				strings.Join(errs, "; ")),
			expectedMatch: false,
		},
		"mismatched status code and missing message": {
			err: &HTTPError{
				Endpoint:   endpoint1,
				StatusCode: http.StatusForbidden,
				Errors:     errs,
			},
			expectedMessage: fmt.Sprintf(
				"response from %q, status: %d, errors: %s",
				endpoint1,
				http.StatusForbidden,
				strings.Join(errs, "; ")),
			expectedMatch: false,
		},
		"mismatched status code, missing message and errors": {
			err: &HTTPError{
				Endpoint:   endpoint1,
				StatusCode: http.StatusUnauthorized,
			},
			expectedMessage: fmt.Sprintf(
				"response from %q, status: %d",
				endpoint1,
				http.StatusUnauthorized),
			expectedMatch: false,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			assert.Equal(t, tc.expectedMatch, errors.Is(&httpErr, tc.err), "error expectation mismatch")
			assert.EqualError(t, tc.err, tc.expectedMessage)
		})
	}
}
