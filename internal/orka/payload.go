package orka

// respBaseBody generated with https://mholt.github.io/json-to-go/
type respBaseBody struct {
	Message string         `json:"message"`
	Errors  respBodyErrors `json:"errors"`
}

type respBodyError struct {
	Message string `json:"message"`
}

type respBodyErrors []respBodyError

func (r respBodyErrors) toStringsSlice() []string {
	var s []string
	for _, e := range r {
		s = append(s, e.Message)
	}

	return s
}
