export VERSION ?= $(shell (scripts/version 2>/dev/null || echo "dev") | sed -e 's/^v//g')
export REVISION ?= $(shell git rev-parse --short HEAD || echo "unknown")
export BRANCH ?= $(shell git show-ref | grep "$(REVISION)" | grep -v HEAD | awk '{print $$2}' | sed 's|refs/remotes/origin/||' | sed 's|refs/heads/||' | sort | head -n 1)
export BUILT ?= $(shell date -u +%Y-%m-%dT%H:%M:%S%z)
export LATEST_STABLE_TAG := $(shell git -c versionsort.prereleaseSuffix="-rc" tag -l "v*.*.*" --sort=-v:refname | awk '!/rc/' | head -n 1)
export CGO_ENABLED ?= 0
export GOPATH ?= ".go/"
export BUILD_PLATFORMS ?= -osarch 'linux/amd64 linux/386' \
                          -osarch 'windows/amd64 windows/386' \
                          -osarch 'darwin/amd64' \
                          -osarch 'freebsd/amd64 freebsd/386'

export goJunitReport ?= $(GOPATH)/bin/go-junit-report
MOCKERY_VERSION = 1.1.0
MOCKERY = .tmp/mockery-$(MOCKERY_VERSION)
gox ?= $(GOPATH)/bin/gox

RELEASE_INDEX_GEN_VERSION ?= master
releaseIndexGen ?= .tmp/release-index-gen-$(RELEASE_INDEX_GEN_VERSION)

PKG := $(shell go list .)
PKGs := $(shell go list ./... | grep -vE "^/vendor/")

GO_LDFLAGS := -X $(PKG).VERSION=$(VERSION) \
              -X $(PKG).REVISION=$(REVISION) \
              -X $(PKG).BRANCH=$(BRANCH) \
              -X $(PKG).BUILT=$(BUILT) \
              -s -w

.PHONY: compile
compile:
	go build \
			-o build/autoscaler \
			-ldflags "$(GO_LDFLAGS)" \
			./cmd/autoscaler

.PHONY: compile_all
compile_all: $(gox)
	# Building project in version $(VERSION) for $(BUILD_PLATFORMS)
	$(gox) $(BUILD_PLATFORMS) \
			-ldflags "$(GO_LDFLAGS)" \
			-output="build/autoscaler-{{.OS}}-{{.Arch}}" \
			./cmd/autoscaler

export testsDir = ./.tests

.PHONY: tests
tests: $(testsDir) $(goJunitReport)
	@./scripts/tests normal

.PHONY: tests_race
tests_race: $(testsDir) $(goJunitReport)
	@./scripts/tests race

.PHONY: lint
lint: TOOL_VERSION ?= 1.27.0
lint: OUT_FORMAT ?= colored-line-number
lint: LINT_FLAGS ?=
lint:
	@if command -v golangci-lint > /dev/null; then \
		if ! $$(golangci-lint --version | grep "version $(TOOL_VERSION)" >/dev/null); then \
		    echo "WARNING: The installed version of golangci-lint is not the recommended version $(TOOL_VERSION)!"; \
		    golangci-lint --version; \
		fi; \
		golangci-lint run ./... --out-format $(OUT_FORMAT) $(LINT_FLAGS); \
	else \
	    echo "golangci-lint does not seem to be installed. Please install it at https://github.com/golangci/golangci-lint#install"; \
	fi

$(testsDir):
	# Preparing tests output directory
	@mkdir -p $@

.PHONY: fmt
fmt:
	# Fixing project code formatting...
	@go fmt $(PKGs) | awk '{if (NF > 0) {if (NR == 1) print "Please run go fmt for:"; print "- "$$1}} END {if (NF > 0) {if (NR > 0) exit 1}}'

.PHONY: mocks
mocks: $(MOCKERY)
	# Removing existing mocks
	@rm -rf ./mocks/ ./internal/mocks/
	# Generating new mocks
	@$(MOCKERY) -recursive -all -keeptree -outpkg mocks -output ./mocks/ -dir ./
	@rm -rf ./mocks/internal
	@$(MOCKERY) -recursive -all -keeptree -outpkg mocks -output ./internal/mocks/ -dir ./internal/

.PHONY: check_mocks
check_mocks:
	# Checking if mocks are up-to-date
	@$(MAKE) mocks
	# Checking the differences
	@git --no-pager diff --compact-summary --exit-code -- \
		$(shell git ls-files mocks/ internal/mocks/) && \
		!(git ls-files -o | xargs realpath | grep 'mocks/' | grep -v "$(GOPATH)") && \
		echo "Mocks up-to-date!"

.PHONY: prepare_ci_image
prepare_ci_image: CI_IMAGE ?= autoscaler-ci-image
prepare_ci_image: CI_REGISTRY ?= ""
prepare_ci_image:
	# Building the $(CI_IMAGE) image
	@docker build \
		--pull \
		--no-cache \
		--build-arg GO_VERSION=$(GO_VERSION) \
		--build-arg GO_SHASUM=$(GO_SHASUM) \
		--build-arg UBUNTU_VERSION=$(UBUNTU_VERSION) \
		-t $(CI_IMAGE) \
		-f dockerfiles/ci/Dockerfile dockerfiles/ci/
ifneq ($(CI_REGISTRY),)
	# Pushing the $(CI_IMAGE) image to $(CI_REGISTRY)
	@docker login --username $${CI_REGISTRY_USER} --password $${CI_REGISTRY_PASSWORD} $(CI_REGISTRY)
	@docker push $(CI_IMAGE)
	@docker logout $(CI_REGISTRY)
else
	# No CI_REGISTRY value, skipping image push
endif

.PHONY: release_docker
release_docker: RELEASE_IMAGE_BASE ?= autoscaler
release_docker: RELEASE_IMAGE_TAG ?=
release_docker: CI_REGISTRY ?= ""
release_docker: GITLAB_RUNNER_VERSION ?= bleeding
release_docker:
	# Building the $(RELEASE_IMAGE) image
	@docker build \
		--pull \
		--no-cache \
		--build-arg GITLAB_RUNNER_VERSION=$(GITLAB_RUNNER_VERSION) \
		-t $(RELEASE_IMAGE_BASE):$(RELEASE_IMAGE_TAG) \
		-f dockerfiles/release/Dockerfile build
ifneq ($(CI_REGISTRY),)
	# Pushing the $(RELEASE_IMAGE) image to $(CI_REGISTRY)
	@docker login --username $${CI_REGISTRY_USER} --password $${CI_REGISTRY_PASSWORD} $(CI_REGISTRY)
	@docker push $(RELEASE_IMAGE_BASE):$(RELEASE_IMAGE_TAG)
ifeq ($(shell git describe --exact-match --match $(LATEST_STABLE_TAG) >/dev/null 2>&1; echo $$?), 0)
	@docker tag $(RELEASE_IMAGE_BASE):$(RELEASE_IMAGE_TAG) $(RELEASE_IMAGE_BASE):latest
	@docker push $(RELEASE_IMAGE_BASE):latest
endif
	@docker logout $(CI_REGISTRY)
else
	# No CI_REGISTRY value, skipping image push
endif

.PHONY: release_s3
release_s3: CI_COMMIT_REF_NAME ?= $(BRANCH)
release_s3: CI_COMMIT_SHA ?= $(REVISION)
release_s3: S3_BUCKET ?=
release_s3:
	@$(MAKE) index_file
ifneq ($(S3_BUCKET),)
	@$(MAKE) sync_s3_release S3_URL="s3://$(S3_BUCKET)/$(CI_COMMIT_REF_NAME)"
ifeq ($(shell git describe --exact-match --match $(LATEST_STABLE_TAG) >/dev/null 2>&1; echo $$?), 0)
	@$(MAKE) sync_s3_release S3_URL="s3://$(S3_BUCKET)/latest";
endif
endif

.PHONY: sync_s3_release
sync_s3_release: S3_URL ?=
sync_s3_release:
	# Syncing with $(S3_URL)
	@aws s3 sync build "$(S3_URL)" --acl public-read

.PHONY: remove_s3_release
remove_s3_release: CI_COMMIT_REF_NAME ?= $(BRANCH)
remove_s3_release: S3_BUCKET ?=
remove_s3_release:
ifneq ($(S3_BUCKET),)
	@aws s3 rm "s3://$(S3_BUCKET)/$(CI_COMMIT_REF_NAME)" --recursive
endif

.PHONY: index_file
index_file: export CI_COMMIT_REF_NAME ?= $(BRANCH)
index_file: export CI_COMMIT_SHA ?= $(REVISION)
index_file: $(releaseIndexGen)
	# generating index.html file
	@$(releaseIndexGen) \
				-working-directory build/ \
				-project-version $(VERSION) \
				-project-git-ref $(CI_COMMIT_REF_NAME) \
				-project-git-revision $(CI_COMMIT_SHA) \
				-project-name "GitLab Runner - Custom Executor's autoscaling driver" \
				-project-repo-url "https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler" \
				-gpg-key-env GPG_KEY \
				-gpg-password-env GPG_PASSPHRASE

.PHONY: check_modules
check_modules:
	@git diff go.sum > /tmp/gosum-$${CI_JOB_ID}-before
	@go mod tidy
	@git diff go.sum > /tmp/gosum-$${CI_JOB_ID}-after
	@diff -U0 /tmp/gosum-$${CI_JOB_ID}-before /tmp/gosum-$${CI_JOB_ID}-after

$(MOCKERY): OS_TYPE ?= $(shell uname -s)
$(MOCKERY): DOWNLOAD_URL = "https://github.com/vektra/mockery/releases/download/v$(MOCKERY_VERSION)/mockery_$(MOCKERY_VERSION)_$(OS_TYPE)_x86_64.tar.gz"
$(MOCKERY):
	# Installing $(DOWNLOAD_URL) as $(MOCKERY)
	@mkdir -p $(shell dirname $(MOCKERY))
	@curl -sL "$(DOWNLOAD_URL)" | tar xz -O mockery > $(MOCKERY)
	@chmod +x "$(MOCKERY)"

$(goJunitReport):
	# Installing go-junit-report
	@go get github.com/jstemmer/go-junit-report

$(gox):
	# Installing gox
	@go get github.com/mitchellh/gox

$(releaseIndexGen): OS_TYPE ?= $(shell uname -s | tr '[:upper:]' '[:lower:]')
$(releaseIndexGen): DOWNLOAD_URL = "https://storage.googleapis.com/gitlab-runner-tools/release-index-generator/$(RELEASE_INDEX_GEN_VERSION)/release-index-gen-$(OS_TYPE)-amd64"
$(releaseIndexGen):
	# Installing $(DOWNLOAD_URL) as $(releaseIndexGen)
	@mkdir -p $(shell dirname $(releaseIndexGen))
	@curl -sL "$(DOWNLOAD_URL)" -o "$(releaseIndexGen)"
	@chmod +x "$(releaseIndexGen)"
